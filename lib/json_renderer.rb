require 'rouge/plugins/redcarpet'
require 'json'

module JsonElementDef
  # Get the childrens from a text/contents
  # @param content [String]
  # @return [Array<Hash>]
  def children_from(content)
    content = content.strip
    return [{ text: content }] unless content.start_with?('{') && content.end_with?('},')
    return JSON.parse("[#{content[0..-2]}]")
  rescue
    [{ text: content }]
  end

  # Finish the document rendering
  def postprocess(full_document)
    return "[#{full_document[0..-2]}]"
  end

  # Show a block code with the right highlighting
  def block_code(code, language)
    { type: :pre, language: language, children: [{ text: code.sub(/\n$/, '') }]}.to_json << ','
  end

  # Show a block quote
  def block_quote(quote)
    { type: :blockquote, children: children_from(quote) }.to_json << ','
  end

  # Show the foot notes block
  def footnotes(content)
    { type: :footernotes, children: children_from(content) }.to_json << ','
  end

  # Show a foot note
  def footnote_def(content, number)
    { type: :footernote, number: number, children: children_from(content) }.to_json << ','
  end

  # Show a header
  def header(text, header_level)
    ref = create_header_ref(text, header_level)
    { type: "h#{header_level}", id: ref, children: children_from(text) }.to_json << ','
  end

  # Show a list block
  def list(contents, list_type)
    if list_type == :ordered
      { type: :ol, children: children_from(contents) }.to_json << ','
    else
      { type: :ul, children: children_from(contents) }.to_json << ','
    end
  end

  def list_item(text, list_type)
    { type: :li, children: children_from(text) }.to_json << ','
  end

  NO_PARAGRAPH_TYPES = %w[figure warning info danger youtube]
  # Show a paragraph
  def paragraph(text)
    children = children_from(text)
    return children[0].to_json << ',' if children.size == 1 && NO_PARAGRAPH_TYPES.include?(children[0]['type'])
    if children.any? { |child| NO_PARAGRAPH_TYPES.include?(child['type']) }
      accu = []
      result = ''
      children.each do |child|
        if NO_PARAGRAPH_TYPES.include?(child['type'])
          if accu.any?
            result << { type: :p, children: accu }.to_json << ','
            accu.clear
          end
          result << child.to_json << ','
        else
          accu << child
        end
      end
      result << { type: :p, children: accu }.to_json << ',' if accu.any?
      return result
    end

    { type: :p, children: children }.to_json << ','
  end

  # Show a table
  def table(header, body)
    children = []
    children << { type: :thead, children: children_from(header) } if header && header.size > 0
    children << { type: :tbody, children: children_from(body) }
    { type: :table, children: children }.to_json << ','
  end

  # Show a table row
  def table_row(content)
    { type: :tr, children: children_from(content) }.to_json << ','
  end

  # Show a table cell
  def table_cell(content, alignment, header = false)
    { type: header ? :th : :td, alignment: alignment, children: children_from(content) }.to_json << ','
  end

  def extend_inline(text, **kwargs)
    return { text: text, **kwargs }.to_json << ',' unless text.start_with?('{') && text.end_with?('},')
    JSON.parse(text[0..-2]).merge(kwargs).to_json << ','
  rescue
    { text: text, **kwargs }.to_json << ','
  end

  def codespan(code)
    extend_inline(code, code: true)
  end

  def double_emphasis(text)
    extend_inline(text, strong: true)
  end

  def triple_emphasis(text)
    extend_inline(text, strong: true)
  end

  def emphasis(text)
    extend_inline(text, em: true)
  end

  def linebreak
    { text: '', br: true }.to_json << ','
  end

  def link(link, title, content)
    { type: :a, url: link, title: title, children: children_from(content) }.to_json << ','
  end

  def raw_html(html)
    { text: html }.to_json << ','
  end

  def strikethrough(text)
    extend_inline(text, del: true)
  end

  def superscript(text)
    extend_inline(text, sup: true)
  end

  def underline(text)
    extend_inline(text, u: true)
  end

  def highlight(text)
    extend_inline(text, mark: true)
  end

  def quote(text)
    extend_inline(text, q: true)
  end

  def footnote_ref(number)
    { type: :footernote_ref, number: number, children: [{ text: '' }] }.to_json << ','
  end

  def normal_text(text)
    return '' if text.strip.empty? || text.strip == '!'
    extend_inline(text)
  end

  # Show an image
  # ![alt text](https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png "Logo Title Text 1")
  # ![alt text|align](url "figcaption")
  def image(link, title, alt_text)
    if alt_text.include?('|')
      alt_text, align = alt_text.split('|')
      return {
        type: :figure,
        src: link,
        alt: alt_text,
        align: align,
        children: [{ text: title }],
      }.to_json << ','
    elsif link.include?('https://www.youtube.com/watch?v=')
      return {
        type: :youtube,
        url: link,
        children: [{ text: '' }]
      }.to_json << ','
    elsif alt_text.casecmp?('tip')
      children = JSON.parse(@sub_render.render(title))
      return {
        type: link,
        children: children,
      }.to_json << ','
    else
      return {
        type: :img,
        src: link,
        alt: alt_text,
        title: title || alt_text,
        children: [{ text: '' }]
      }.to_json << ','
    end
  end
end

class JsonRenderer < Redcarpet::Render::HTML
  include Rouge::Plugins::Redcarpet
  include JsonElementDef

  class SubRenderer < Redcarpet::Render::HTML
    include JsonElementDef
    def paragraph(text)
      children = children_from(text)
      { type: :subP, children: children }.to_json << ','
    end
  end
  # Constant containing the Summary title in various langs
  SUMMARY_TITLES = {
    'fr' =>  'Sommaire',
    'en' => 'Contents',
  }
  # @return [String] title of the page
  attr_reader :title

  # Initialize the renderer variables
  def init(lang)
    @categories = {}
    @header_counters = [0, 0, 0, 0]
    @summary_title = SUMMARY_TITLES[@lang = lang] || 'Contents'
    @sub_render ||= Redcarpet::Markdown.new(SubRenderer.new)
  end

  # Create a new header ref
  def create_header_ref(text, header_level)
    if header_level == 1
      @title = text
      return 'top'
    end
    @header_counters[header_level - 2] += 1
    (header_level - 1).upto(3) { |i| @header_counters[i] = 0 }
    ref = "cat_#{@header_counters[0, header_level - 1].join('_')}"
    @categories[ref] = text
    ref
  end
end

# Event Making

This section of the Wiki contain all the tutorials related to Event Making under Pokémon SDK.

Some basic information can be presented here.

**Basic commands**:
- [Show a message](messages.md)
- [Show a choice](choices.md)

**Events** related commands:
- [Edit Event attributes](event_attribute.md)

**Pokémon** related commands:
- [Give a Pokemon](give_pokemon.md)
- [Release a roaming Pokémon](roaming_pokemon.md)
- [Open a Pokemon Shop](create_pokemon_mart.md)
- [Create a starter choice](create_starter_choice.md)

**Items** related commands:
- [Give an item](give_item.md)
- [Enable the Pokedex](enable_pokedex.md)
- [Open a Shop](create_mart.md)
- [Give a badge](give_badge.md)

**Non Playable Characters (NPC)** related commands:
- [Create a Pokémon Center](create_pokemon_center.md)

**Battle! Wild Pokémon** related commands:
- [Start a wild battle](start_wild_battle.md)

**Battle! Trainers** related commands:
- [Create a Trainer event](trainer_event.md)

**Quests** related commands:
- [Manage quests](quest.md)

**Systems** related commands:
- [How to use the FollowMe system](followme.md)
- [Manage Time system](time-system.md)
- [Launch the Hall of Fame](hall_of_fame.md)
- [Launch/Configurate the Mining Game](mining_game.md)
# How to use the FollowMe system

Since 2014 PSDK has a really interesting Following System! This system allows the player to be followed by :
- Its Pokémon
- Its friends (Heroes in the RMXP data base from 2 upto infinity)
- Its friend's Pokémon (`$storage.other_party`)

It's allso possible to make an event following another event.

## Management of the Following System
### Activation / Deactivation.

To activate the following system, switch the switch `19` "**FollowMe Actif**" on. To disable it just switch off the same switch.

### Temporary deactivation

If for any reason (like surf) you need to temporarilly disable the Following System, just write this in the events :
- deactivation : `Yuki::FollowMe.smart_disable`
- re-activation : `Yuki::FollowMe.smart_enable` (won't enable the system if it was disabled before `smart_disable`).

Note : If you don't want the following System to re-enable when the player is on by, use this :
```ruby
Yuki::FollowMe.smart_enable unless $game_switches[::Yuki::Sw::EV_Bicycle] || $game_switches[::Yuki::Sw::EV_AccroBike]
```

### Define the number of Trainer's following Pokemon

To define the number of your Pokémon who'll be following you, set the right value in the variable `19` "**Nb Pokémon Suiveurs**". 0 = no Pokémon, 1 = the first, 2 = the first & the second etc...

### Define the number of friend who's following you

The variable `18` "**Nb Humain Suiveurs**" define the number of friend (Heroes from RMXP database starting from 2) who's following you.

### Define the number of your Friend's Pokémon following you

Use the variable `20` "**Nb Pkm Ami Suiveurs**". It works like the variable `19`.

## Move a follower

If for any reason you need to move a follower, change the variable `21` "**Follower Selectionné**" and move the player (the selected follower will be the first if variable 21 is equal to 1, the second if 2 etc...)

![tip](info "If you set this variable to `-1` the follower won't move if you move the player.")

![tip](warning "Never forget to set the variable `21` to `0`, otherwise the FollowMe won't work properly.")

## Change the Z property of the player an its followers

To prevent graphic glitches with the bridges, it's recommanded to hide the `z` property change (4 <-> 0) in a event in strairs using this command : 
```ruby
$game_player.change_z_with_follower(z)
```

It's recommanded to replace `z` by 4 when you need the player to cross a bridge. And set `z` to 0 if you want him to get back to the regular layer.

## Use the FollowMe on events

All [`Game_Character`](https://psdk.pokemonworkshop.fr/yard/Game_Character.html) objects can follow other [`Game_Character`](https://psdk.pokemonworkshop.fr/yard/Game_Character.html) objects. Call the `set_follower` of the followed event to make another event follow it.

Example, Event 001 following Event 002 :
```ruby
get_character(2).set_follower(get_character(1))
```

Example, Event 005 following the player (player's Pokémon will follow the event) :
```ruby
$game_player.set_follower(get_character(5))
```

![tip](warning "When an event follows the Player, you should aways make sure it stops following the player before transfering to another map.")

## Unlink event

To make event stops following the player, use the following command:
```ruby
$game_player.reset_follower
```

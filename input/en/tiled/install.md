# Install Tiled

Let's begin those tutorials about Tiled software !  

As a reminder, **Tiled is a 2D level editor that helps you develop the content of your game**, developped by *Thorbjørn Lindeijer* since 2008. Its primary feature is to edit tile maps of various forms, but it also supports free image placement as well as powerful ways to annotate your level with extra information used by the game. Tiled focuses on general flexibility while trying to stay intuitive.

## Download Tiled Map Editor

Tiled can be download at the [official website](https://www.mapeditor.org/). Choose [Download at itch.io](https://thorbjorn.itch.io/tiled) then `Download`. A window will pop up. You can contribute to help the development or just get it free. Then, choose `No thanks, show me the download links.`.

## Install Tiled

Choose the version regarding your Operating System (OS) and then regarding your choice, 
Sélectionnez la version qui correspond à votre Système d'Exploitation (OS) et selon votre choix, let the installer guide you. If you want so, snapshots are available for each OS and let you install Tiled easily by just putting the folder on your computer.

![tip](warning "However, avoid typical sensitive folders like System32 on Windows.")

### Update Tiled

When an update is available, you will have to repeat the operation in order to get it. To avoid having to monitor the website constantly, a function is available in Tiled. Choose `Edit` in the menu, then `Preferences`, in `Interface` check the `Highlight new version in status bar` option, just like on the following screenshot.

![Highlight new version of Tiled](img/tiled/tiled_nouveautes_en.png)

## Use Tiled

Once the installation is done, launch it to test if everything is okay. You can now open a Tiled project, a Tiled file, create a new Map or a new Tileset.

![First use of Tiled](img/tiled/tiled_premiere_ouverture_en.png)
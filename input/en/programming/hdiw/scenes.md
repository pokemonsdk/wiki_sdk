# Scenes (GamePlay)

PSDK was written from RPG Maker XP base. This mean the most basic scene protocol is working : making a class whose objects respond to `main`. With the evolution of PSDK we created a class called `GamePlay::Base` and started to specialize all the scenes from that. The goal of `GamePlay::Base` is to manage most aspect of the scenes :
- Calling the update loop
- Managing messages (`display_message`)
- Calling other scenes
- Calling dispose

Today we use `GamePlay::BaseCleanUpdate` which derives from `GamePlay::Base` and we would only have to define the following things :
- `initialize`
- `create_graphics`
- `update_inputs`

The rest is managed or optional. To explain some methods, we'll explain the cycle of life of a `GamePlay::BaseCleanUpdate` child object.

## Cycle of life

Here's a simplified diagram of the cycle of life :
![diagram|center](img/programming/hdiw/gpbcu.png "Cycle of life diagram")

The blue rectangles are methods you should never define yourself. The `dispose` method will dispose all the viewports that are stored in an instance variable containing the word viewport in their name. You can overwrite it to dispose bitmaps if you didn't used the cache, don't forget to call the super when you do that.

### initialize

The first thing to do is defining `initialize`. This method can accept any parameter as long as they help to make the scene works properly. The `initialize` method **has to** call the `super()` (it creates the message window and try to prevent softlock from messages that wasn't terminated before calling the scene).

In the `initialize` method, the state of the scene should be the only thing that are created, graphics, viewport etc... should never be created inside `initialize`.

### create_graphics

The second thing to do is defining `create_graphics`, it's better when the scene looks like something. This method is automatically called by `GamePlay::Base` so you don't need to call it explicitely.

![tip](warning "When you define `create_graphics` don't forget to call `super` or `create_viewport`. The `create_graphics` from GamePlay just calls `create_viewport`")

### create_viewport

Usually, you'll never call `create_viewport` unless you need more than one viewport. The default `create_viewport` create the `main` viewport inside the `@viewport` variable. If you need more viewport, use instance_variables that contains the word `viewport` in it.

![tip](info "All the variables that contains `viewport` in their name will receive the `dispose` message from the `dispose` method.")

### update_inputs

In `GamePlay::BaseCleanUpdate` childs, `update_inputs` should be a public method. It's called automatically by `update` if no message are shown. You'll manage all the input there and return a boolean to tell if the mouse interactions should be updated or not.

### update_mouse

In `GamePlay::BaseCleanUpdate` childs, `update_mouse` should be a public method taking the `moved` boolean argument. It's called automatically by `update` if no message are shown and `update_inputs` didn't returned `false`. You'll manage all the mouse interactions here and return a boolean that will conditionate the return of `update`.

### update_graphics

In `GamePlay::BaseCleanUpdate` childs, `update_graphics` should be a public method. It's called each frame regardless of the state of the scenes. You should update stuff like animations here. This method should return a boolean that will conditionate the return of `update`.

## Example of a scene that does nothing

To demonstrate the simpliness of defining a scene, here's a script that only show a random number and quit if you press the back button:

```ruby
class MyScene < GamePlay::BaseCleanUpdate
  def initialize
    super()
    @random_number = rand(0xFFFFFFFF)
  end

  def update_inputs
    return @running = false if Input.trigger?(:B)
    return true
  end

  private

  def create_graphics
    super # Create the @viewport variable
    create_text
  end

  def create_text
    @text = Text.new(0, @viewport, 0, 0 - Text::Util::FOY, 320, 16, @random_number.to_s, 1)
  end
end
```

## How to call another scene

To call another scene, you'll need to call `call_scene(Scene, *args)`.
- `Scene` is the class of the scene to call.
- `*args` is all the argument to pass to `Scene.new`.

The `call_scene` method accepts a block with the parameter `scene` (that is the instance of the scene that was called). This block is called once the scene was disposed and is usefull to retreive the result of calling the scene (eg. choosen Pokemon).

Example : 
```ruby
call_scene(GamePlay::Save) do |scene|
  @running = false if scene.saved
end
```
It'll call the `GamePlay::Save` scene without arguments and will stop the current scene if the player saved (according to GamePlay::Save documentation).

## How to return to Scene_Map
Some Menu Scene would like to return to `Scene_Map` (for some reason), if all the previous scenes used `call_scene` the best way to return to `Scene_Map` is to call `return_to_scene(Scene_Map)`. 

When `call_scene` is used by the previous scene, if `return_to_scene` is used by the current scene, the transition to the previous scene will not be performed unless it's `Scene_Map`. (This allow to skip various transitions).

![tip](info "When `@running` is set to false by the `call_scene` block parameter, the transition to the current scene is also not performed.")
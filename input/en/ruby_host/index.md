# Ruby Host

![tip](warning "This page comes from the **old Wiki**. It's probably not up to date.")

RubyHost is the current database editor for PSDK!
It was built using Visual Studio 2012 and is becoming old, we plane on replacing it with something else but as long as we don't have a replacement for it, we'll use RubyHost.

![Home UI|center](img/ruby_host/Rubyhost_main.png "RubyHost's Home UI")

## The editors

RubyHost has variouse editor used to manipulate PSDK data :

- [Pokémon editor](pokemon.md)
- [Move editor](move.md)
- [Item editor](item.md)
- [Zone editor](zone.md)
- [Text editor](text.md)
- [Type editor](type.md)
- [Trainer editor](trainer.md)
- [Quests editor](quest.md)
- [MapLinker editor](maplinker.md)

## Additional tutorials

- [Create a Trainer Class](create_trainer_class)

![tip](info "Abilities are scripted thus the Ability button only update the ability list.")
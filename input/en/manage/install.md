# Install PSDK

In this article we'll see how to install PSDK, if you don't want to read, you can watch buttjuice youtube video about PSDK :
![video](https://www.youtube.com/watch?v=geabEY75Zmo "Buttjuice's Video")

## Download PSDK

You have two way to download PSDK. You can grab an archive from [the download page](https://download.psdk.pokemonworkshop.com "PSDK Download Page") or from the [Pokémon Workshop Discord](https://discord.gg/0noB0gBDd91B8pMk).

### Download from the Pokémon Workshop Discord

In the Pokémon Workshop Discord there's a `#access_psdk` channel where you ask for Access.

![Access PSDK|center](img/manage/access_psdk.png "Request for PSDK Access")

Once you got the access, some channel will appear :
- `#windows` : You'll find the PSDK archive in this channel
- `#linux` : Same but for Linux Users (not really active since there's not a lot of Linux users)
- `#git` : Contain all the git news (Merge Request & push on development), you can mute this channel
- `#ruby-host` : Contain the link to the English Ruby Host version
- `#ressources` : Contain some archives that may interest you (Uncompiled Pokémon Sprites, X/Y & ORAS Battlers etc...)

## Install PSDK

![tip](danger "You may be tempted to extract PSDK with `WinRaR`. You should not do this. `WinRaR` and `7zip` are two russian **competing** software, one cost money and the other is free and they don't understand their competitor format. This mean you need to use [7zip](https://www.7-zip.org) to extract PSDK otherwise you'll have some bugs.")

Extract PSDK in a folder that match the following requirement :
- The folder is **world writable** : This mean PSDK can update itself when you ask it.
- The folder path does only contain the following list of characters : `abcdefghijklmnopqrstuvwxyz _/.\0123456789` (any other character will lead issues for Running PSDK, upcase characters are ok).

Examples :
- C:/PSDK : **works**
- C:/Windows/System32/PSDK : Does **not** work (not world writable)
- C:/Users/my_user_name/Desktop/PSDK : **works**
- C:/Users/mÿ_ûsèr_ñame/Desktop/PSDK : Does **not** work (bad characters)

### Test if PSDK works

To test if PSDK works perform the following steps :
1. open `cmd.bat`
2. write `game`
3. press enter

If the Game window opens and the game seem to work properly, you can close it.  
If the Game window does not show, report the console log in `#english-support` on [Pokémon Workshop Discord server](https://discord.gg/0noB0gBDd91B8pMk).

### Update PSDK

Most of the time, the full archives are not up-to-date. You need to update them using the launcher.
1. Open Launcher-english.exe
2. Click on `Detect and Download Update`

If the update fails, try the following procedure :
1. open `cmd.bat`
2. write `game --util=update`
3. press enter

You'll be able to copy & paste the whole console log in `#english-support` on [Pokémon Workshop Discord server](https://discord.gg/0noB0gBDd91B8pMk)

### Set PSDK to english by default

With a simple script you can force PSDK to be in english :
```ruby
module GamePlay
  class Load
    remove_const :DEFAULT_GAME_LANGUAGE
    DEFAULT_GAME_LANGUAGE = 'en'
    LANGUAGE_CHOICE_LIST.clear
  end
end
```

If you want to keep the language choice, just remove the `LANGUAGE_CHOICE_LIST.clear` line.

![tip](info "By default PSDK uses an AZERTY layout, you can modify the key binding by pressing `F1` in game.")

## Edit events & maps

To edit your events & maps, you can open one of the following file with RPG Maker XP :
- `Game_RMXP_1.02.rxproj`
- `Game_RMXP_1.03.rxproj`
- `Game_RMXP_1.05.rxproj`

They work according to your RPG Maker XP version.

![tip](info "If you don't see the .rxproj extensions you should follow this tutorial [Show File Extension in Windows](https://www.thewindowsclub.com/show-file-extensions-in-windows). It's really important to always see the extensions otherwise you could click on a `exe`, `com`, `bat` or `sys` file without knowing it and maybe get a Virus.")

![tip](info "Speaking of Virus, if you analyze the PSDK archive with virus total, some AV may detect a trojan, especially the chinese one. If none of F-Secure, Kaspersky, MalewareByte, BitDefender, Windows Defender or MacAfee detect a virus, you can consider the archive is clean. In case of doubt, always download the archive from Official Sources.")


# Build under Mac OS

## Warning

Mac OS is an OS where you're normally not building stuff like in Linux or Windows. This tutorial will then involve several trick and hack to make sure it works. Even if it works you may encounter some issues, we tried to fix most of them with a cool member called Lynn (Ayukito on github). 

The currently unsolved issue is the incompatibility of some audio files and Pokémon ability-related crashes.

![tip](warning "This page will probably get updated once we find a better way to do these things")

## Downloading PokemonSDK

You should download PSDK from #mac on the Pokémon Workshop server.

## Installing SFML

SFML is required to use LiteRGSS and SFMLAudio. Lynn has created a script below that automatically downloads and installs it.
Save a file called `SFML.command` with the following contents and run it.
```
#! /bin/bash

BASEDIR=$(dirname "$0")

cd "$BASEDIR"

clear

curl -O https://www.sfml-dev.org/files/SFML-2.5.1-macOS-clang.tar.gz

tar -xvf SFML-2.5.1-macOS-clang.tar.gz

cd SFML-2.5.1-macos-clang

sudo mv -f Frameworks/* /Library/Frameworks

sudo mv -f extlibs/* /Library/Frameworks

sudo mv -f lib/* /usr/local/lib

sudo mv -f include/* /usr/local/include

cd ..

rm SFML-2.5.1-macOS-clang.tar.gz
rm -r SFML-2.5.1-macos-clang
```

## Compiling the LiteRGSS

Here's the instruction from Lynn to do so:

```
#####################################################################
Skip steps 1-3 if you already have brew, brew ruby, and rake-compiler
#####################################################################
1. Install brew
Open terminal and run the following command
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"

2. Install Ruby
In terminal, run the following command
brew install ruby

For safe measures, run the following commands too:

echo 'export PATH="/usr/local/opt/ruby/bin:$PATH"' >> ~/.zshrc
export LDFLAGS="-L/usr/local/opt/ruby/lib"
export CPPFLAGS="-I/usr/local/opt/ruby/include"

3. Install rake-compiler
Completely close out of your terminal and re-open it
Run the following command
gem install rake-compiler

4. Clone LiteRGSS
Run the following command
git clone https://github.com/NuriYuri/LiteRGSS.git

#########################################
SKIP STEPS 5-7 IF YOU INSTALLED SFML ABOVE!
#########################################

5. Download SFML 2.5.1 for Mac
Run the following command
curl -O https://www.sfml-dev.org/files/SFML-2.5.1-macOS-clang.tar.gz
And then uncompress it by doing the following command
tar -xvf SFML-2.5.1-macOS-clang.tar.gz

6. Move the SFML library into LiteRGSS
Run the following command
mv SFML-2.5.1-macos-clang/include/SFML LiteRGSS/ext/LiteRGSS

6.5 Install the dependancies for SFML
Follow the instructions here to move all the dependancies for SFML. If you can't see hidden files, press Shift + Command + Period (. Or dot)

https://www.sfml-dev.org/tutorials/2.5/start-osx.php

7. Cleaning up SFML
We will now remove the download and extracted SFML files
Run the following commands

rm SFML-2.5.1-macOS-clang.tar.gz
rm -r SFML-2.5.1-macos-clang

8. Building LiteRGSS
CD into LiteRGSS by entering the following command
cd LiteRGSS

Then type the following command
rake compile

You may see errors regarding LiteRGSS_test.o, that is normal. The tests aren't updated to compile with Mac yet.

You will find your compiled LiteRGSS.bundle file inside of LiteRGSS/lib if everything works out.
```

Don't forget that in order to make PSDK load the LiteRGSS you will have to copy the file `LiteRGSS.bundle` to the root of the PSDK project (where `Game.rb` stands).


## Audio

PSDK supports both SFMLAudio and FMod sound. FMod supports more formats but SFMLAudio is easier to install the dependancies for on MacOS.
It will prioritize FMod over SFMLAudio if both *.bundle files are found.

## Compiling the SFMLAudio

Here's the instruction from Lynn to do so:

```
#####################################################################
Skip steps 1-3 if you already have brew, brew ruby, and rake-compiler
#####################################################################

1. Install brew
Open terminal and run the following command
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"

2. Install Ruby
In terminal, run the following command
brew install ruby

For safe measures, run the following commands too:

echo 'export PATH="/usr/local/opt/ruby/bin:$PATH"' >> ~/.zshrc
export LDFLAGS="-L/usr/local/opt/ruby/lib"
export CPPFLAGS="-I/usr/local/opt/ruby/include"

3. Install rake-compiler
Completely close out of your terminal and re-open it
Run the following command
gem install rake-compiler

4. Clone SFMLAudio
Run the following command
git clone https://gitlab.com/NuriYuri/sfmlaudio.git

#########################################
SKIP STEPS 5-7 IF YOU INSTALLED SFML ABOVE!
#########################################

5. Download SFML 2.5.1 for Mac
Run the following command
curl -O https://www.sfml-dev.org/files/SFML-2.5.1-macOS-clang.tar.gz
And then uncompress it by doing the following command
tar -xvf SFML-2.5.1-macOS-clang.tar.gz

6. Move the SFML library into SFMLAudio
Run the following command
mv SFML-2.5.1-macos-clang/include/SFML SFMLAudio/ext/SFMLAudio

6.5 Install the dependancies for SFML
Follow the instructions here to move all the dependancies for SFML. If you can't see hidden files, press Shift + Command + Period (. Or dot)

https://www.sfml-dev.org/tutorials/2.5/start-osx.php

7. Cleaning up SFML
We will now remove the download and extracted SFML files
Run the following commands

rm SFML-2.5.1-macOS-clang.tar.gz
rm -r SFML-2.5.1-macos-clang

8. Building LiteRGSS
CD into LiteRGSS by entering the following command
cd LiteRGSS

Then type the following command
rake compile

You will find your compiled SFMLAudio.bundle file inside of SFMLAudio/lib if everything works out.
```

Don't forget that in order to make PSDK load the SFMLAudio you will have to copy the file `SFMLAudio.bundle` to the root of the PSDK project (where `Game.rb` stands).

## Compiling FMOD

Here's Lynn's instruction to do so:

```
1. Downloading fmod

Go to https://www.fmod.com/download and create an account, then go back to the download page.
Click FMOD Studio Suite
Scroll down to FMOD Engine
Click "Older" for the version.
Make sure the dropdown says "1.10.20"
Click the download button next to Mac

It should download a .Dmg file. Open it.

2. Moving the required files for compilation and usage
Open "FMOD Programmers API"
Open "api"
Open "lowlevel"
Open "lib"
Move all files in "lib" to /usr/local/lib

Go back to "lowlevel"

Copy all the files inside of "inc" to a location for you to access later.

3. Clone RubyFmod
git clone https://github.com/NuriYuri/Ruby-Fmod.git
Or download and extract it.
Copy those files from "inc" you moved earlier into "ext/RubyFmod"

4. Compile RubyFmod
In your terminal, cd into RubyFmod
Run the following command:
rake compile

5. You're done. The compiled file is in /lib/RubyFmod.bundle
Copy that to the root of your PSDK game where your game.rb file is (not the sdk)
```

Don't forget that in order to make PSDK load the RubyFmod you may have to copy the file `RubyFmod.bundle` to the root of the PSDK project (where `Game.rb` stands).

## Ruby File Loader

Lynn made a project called `Ruby File Loader`. It should help to make an uncompiled PSDK game run with no issue as long as Ruby was installed from Brew: [link](https://github.com/Ayukito/Ruby-File-Loader)
To use it, place your game folder into `Ruby-File-Loader/Ruby-File-Loader/` and name the game folder Project. (Remove the existing Project folder there)
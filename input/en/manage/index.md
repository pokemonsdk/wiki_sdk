# Manage PSDK

In this section we'll talk about things that helps you to manage PSDK.

## Article list

- [Install PSDK](install.md)
- [Build Under Linux](build-under-linux.md)
- [Configure your Project](configure.md)
- [Install a Script](install-script.md)
- [Fix issues](fix-issues.md)

# Editeur d'Objets

![tip](warning "Cette page provient de l'Ancien Wiki. Elle n'est peut-être pas à jour.")

## Interface des Objets de RubyHost

Le bouton `Objets` accessible sur l'interface d'accueil de RubyHost permet d'accéder à la fenêtre d'édition des données des objets. Cette dernière permet de gérer les données de soin (Potion, Rappel), de capture (Pokéball) et d'autres données spécifiques des différents objets que peut trouver (ou utiliser) le joueur.

![Editeur d'objet|center](img/ruby_host/Rubyhost_itemsmain.png "Interface de l'éditeur d'objets")

## Créer un nouvel objet

Pour créer un nouvel objet :

1. Assurez vous d'avoir correctement ajouté les entrées de texte qui lui correspondent. (Nom + Description)
2. Cliquez sur le bouton `Ajouter` à droite de `Objet :`.
3. Configurez votre objet selon vos choix (Utilisation en combat, sur map, peut être porté par un Pokémon, etc.).
    - Si vous souhaitez que l'objet en question soit `consommé` après utilisation, cochez `Utilisation limitée` (pour une CT ou une CS à usage illimité, décochez cette case).
    - Si vous souhaitez que votre objet ne puisse pas être vendu, laissez 0 dans `Prix :`. L'objet sera considéré comme invendable.
4. Pour un objet qui concerne l'ajout ou retrait de PV, de PP, de Bonheur, de Niveaux, le retrait d'un statut, l'ajout d'EV, un boost de statistique (Potion, Huile, Super Bonbon, Anti-Para, etc.) :
    - Cliquez sur le bouton `Données de soin`.
    - Une fenêtre s'ouvre alors pour vous permettre de modifier ces données.
    - N'oubliez pas de cliquer sur `Valider`.  
        ![Editeur de soin|center](img/ruby_host/Rubyhost_itemheal.png "Edition du soin réalisé par un objet")
5. Pour un objet qui concerne la capture de Pokémon (Poké Ball, Honor Ball, etc.) :
    - Cliquez sur le bouton `Données de capture`.
    - Une fenêtre s'ouvre alors pour vous permettre de modifier ces données.
    - Les Balls spécifiques telles que la Filet Ball sont à programmer dans `Capture spécifique (hash) :` (suivez l'exemple des Balls officielles si vous voulez créer la vôtre).
    - N'oubliez pas de cliquer sur `Valider`.  
        ![Editeur de ball|center](img/ruby_host/Rubyhost_itemcapture.png "Editeur de Pokéball")
6. Pour un objet qui ne concerne pas les deux catégories du dessus mais possède des caractéristiques spécifiques (Pièce Rune, Baies, Orbe Vie, CT, CS, etc.) :
    - Cliquez sur le bouton `Autres données`.
    - Une fenêtre s'ouvre alors pour vous permettre de modifier ces données.
    - N'oubliez pas de cliquer sur `Sauvegarder`.
7. N'oubliez pas de cliquer sur `Sauvegarder` une fois de retour sur l'interface d'édition de l'objet.

## Créer une nouvelle CT ou CS

Pour créer une nouvelle CT ou CS :
1. Assurez vous d'avoir correctement ajouté les entrées de texte qui lui correspondent.
2. Cliquez sur le bouton `Ajouter` à droite de `Objet :`.
3. Configurez votre objet selon vos choix (Utilisation en combat, sur map, peut être porté par un Pokémon, etc.).
4. Si vous souhaitez que l'objet en question soit `consommé` après utilisation, cochez `Utilisation limitée` (pour une CT ou une CS à usage illimité, décochez cette case).
5. Si vous souhaitez que votre objet ne puisse pas être vendu, laissez 0 dans `Prix :`. L'objet sera considéré comme invendable.
6. Cliquez sur le bouton `Autres données`.
    - Vérifiez qu'il s'agisse bien de l'objet en question.
    - Choisissez le nombre pour `ID de CT :` ou `ID de CS :` (en prenant garde à ne pas utiliser un ID déjà pris).
    - Sélectionnez dans la liste déroulante la capacité que doit faire apprendre cette CT ou CS.
    - N'oubliez pas de `Sauvegarder`, puis `Sauvegarder` à nouveau avant de quitter l'interface d'édition des objets.

![tip](info "L'événement commun utile au déclenchement d'une CS est à programmer sur RPG Maker XP. Vous pouvez vous inspirer de l'événement commun de Coupe ou Éclate-Roc par exemple.")
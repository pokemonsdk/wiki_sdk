# Installer Tiled

C'est parti pour cette série de tutoriels sur le logiciel Tiled !  

Pour rappel, **Tiled Map Editor est un logiciel d'édition de cartes (ou niveaux) en deux dimensions (2D)**, développé par *Thorbjørn Lindeijer* depuis 2008, qui vous aide à réaliser le contenu de votre jeu. Il vous permet en premier lieu de créer vos cartes en plaçant simplement des éléments graphiques sur une grille, mais il vous permet également d'ajouter des images dessus (pour décalquer votre map par exemple) ou des métadonnées. Tiled se veut flexible et le plus intuitif possible.

## Télécharger Tiled Map Editor

Tiled est accessible via le [site officiel](https://www.mapeditor.org/). Cliquez sur [Download at itch.io](https://thorbjorn.itch.io/tiled) puis cliquez sur `Télécharger`. Une fenêtre s'ouvre, vous pouvez contribuer à soutenir son développement ou l'obtenir gratuitement. Pour cela, cliquez sur `Non merci, montrez-moi les téléchargements`.

## Installer Tiled

Sélectionnez la version qui correspond à votre Système d'Exploitation (OS) et selon votre choix, laissez-vous guider par l'installateur. Si vous le souhaitez, des snapshots sont disponibles pour chaque OS et vous permettent d'installer Tiled simplement en plaçant le dossier sur votre ordinateur.

![tip](warning "Évitez tout de même les emplacements sensibles type System32 sur Windows.")

### Mettre à jour Tiled

Lorsqu'une mise à jour est disponible, vous devrez répéter l'opération afin d'obtenir celle-ci. Pour vous éviter de devoir surveiller le site internet constamment, une fonction est disponible directement depuis le logiciel. Pour cela, sélectionnez `Édition` dans le menu contextuel, puis `Préférences...`, onglet `Interface`, section `Mises à jour`, puis cochez la case `Surligner la nouvelle version dans la barre d'état`, comme indiqué sur la capture ci-après.

![Afficher les mises à jour de Tiled](img/tiled/tiled_nouveautes.png)

## Utilisation de Tiled

Une fois l'installation terminée, pensez à tester le logiciel en le lançant. Il vous permet alors d'ouvrir un projet Tiled, un fichier Tiled, de créer une nouvelle Carte ou un nouveau Jeu de Tuiles (dit tileset).

![Première ouverture de Tiled](img/tiled/tiled_premiere_ouverture.png)
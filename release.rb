ENV['WIKI_CSS_PATH'] = 'https://image.communityscriptproject.com/psdkwiki/css'
ENV['WIKI_IMG_PATH'] = 'https://image.communityscriptproject.com/psdkwiki/img'
ENV['WIKI_PATH'] = 'https://psdk.pokemonworkshop.fr/wiki'

require_relative 'render.rb'

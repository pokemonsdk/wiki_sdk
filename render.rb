require 'bundler'
Bundler.require(:default)
require_relative 'lib/custom_renderer'
require_relative 'lib/json_renderer'
require_relative 'lib/wiki_generator'

# Setup encoding
Encoding.default_external = Encoding::UTF_8
Encoding.default_internal = Encoding::UTF_8

# Create wiki renderer
wiki_renderer = WikiGenerator.new('wiki.json')

# Generate the files
wiki_renderer.process

# Set ENV['JSON'] to 'true' to render everything in JSON
